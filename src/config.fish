alias h=help.
function help.
  echo
  echo
  echo '  💻    Welcome to dvddcfg, the quickest route to pleasant programming'
  echo
  echo
  echo " System "
  echo "    config.   -   Open fish config"
  echo "    install.  -   Install a package"
  echo "    search.   -   Search for a package"
  echo "    up.       -   Update installed packages"
  echo "    re.       -   Reload fish config"
  echo
  echo
  echo " Network "
  echo "    myip      -   Get your local and public IP addresses"
  echo "    wifion    -   Turn Wi-Fi on or off"
  echo
  echo
  echo " Command Line "
  echo "    b         -   popd"
  echo "    x         -   Clear the terminal"
  echo "    mkd       -   Make and enter directory"
  echo "    rmds      -   Recursively delete .DS_Store files"
  echo
  echo
  echo " Git "
  echo "    ga        -   git add all"
  echo "    gca       -   git commit all"
  echo "    gc        -   git commit"
  echo "    gs        -   git status"
  echo "    gd        -   git diff"
  echo "    gds       -   git diff staged"
  echo "    gsa       -   git status for all repos found in ~/code/"
  echo "    gr       -    git remote show (git remote show)"
  echo
  echo
  echo " Python"
  echo "    py       -   python3"
  echo "    pye      -   Create a venv or activate it"
  echo "    pyd      -   Deactivate a venv"
  echo -e "\n\n"
end

# -----------------------------------------------

alias cfg.=config.
alias conf.=config.
function config.
  echo "Opening config..."
  if type -q code
    code ~/.config/fish/config.fish
  else
    nano ~/.config/fish/config.fish
  end
end


alias ins.=install.
function install.
  switch (uname)
    case Linux
      apt install $argv
    case Darwin
      brew install $argv
  end
end


alias sr.=search.
function search.
  switch (uname)
    case Linux
      apt search $argv
    case Darwin
      brew search $argv
  end
end


alias up.=update.
function update.
  echo 'Updating packages'
  switch (uname)
    case Darwin
      brew update; and brew upgrade
    case Linux
      apt update; and apt upgrade
  end
  if type -q yarn
    echo 'Updating yarn globals'
    yarn global upgrade
  end
  if type -q omf
    echo 'Updating omf'
    omf update
  end
end


# Must be at end if used in other functions, as it starts new shell process
alias re.=reload.
function reload.
  if type -q omf
    echo 'Reloading fish config'
    omf reload
  end
end

#  CLI ------------------------------------------

alias x=clear
alias b=popd


function mkd
  mkdir -p $argv; and cd $argv
end


function rmds
  find . -name '.DS_Store' -type f -delete
end

# Git -------------------------------------------

function ga
  git add .
end

alias gcfg=gconfig
alias gconf=gconfig
function gconfig
  code ~/.gitconfig
end

alias gb=gbranch
function gbranch
  git branch -a
end

alias gr=gremote
function gremote
  # git branch -r to only show remote
  git branch -a
  git remote show (git remote show)
end

function gtag
  git tag $argv[1]
end

alias gl=glog
function glog
  git log --oneline --decorate --all --graph
end

alias gch=gcheckout
alias gcheck=gcheckout
function gcheckout
  git checkout $argv[1]
end

alias gca=gcommitall
function gcommitall
  git commit -am $argv[1]
end

alias gc=gcommit
function gcommit
  git commit -m $argv[1]
end


alias gs=gstatus
function gstatus
  git status
end

alias gsa=gstatusall
function gstatusall
  python3 ~/.config/dvddcfg/scripts/git_status.py
end


alias gd=gdiff
function gdiff
  git diff
end

alias gds=gdiffstaged
function gdiffstaged
  git diff --cached
end


#  Python ---------------------------------------

alias py=python3


function pye
  # Set directory - default: env/
  if test (count $argv) -eq 0
    set pyenvdir "env"
  else
    set pyenvdir $argv[1]
  end

  # Create venv if none exists
  if not test -e $pyenvdir"/bin/activate.fish"
    python3 -m venv $pyenvdir
    echo "Venv created at:" $pyenvdir"/"
  end

  # Activate venv
  if test -e $pyenvdir"/bin/activate.fish"
    source $pyenvdir"/bin/activate.fish"
    echo "Venv active"
  end
end


function pyd
  deactivate
end


# Utilities -------------------------------------

function pxon
  switch (uname)
    case Darwin
      networksetup -setsocksfirewallproxy "Wi-Fi" localhost 8123
      and networksetup -setsocksfirewallproxystate "Wi-Fi" on
      and networksetup -getsocksfirewallproxy "Wi-Fi"
      and echo '  💻   Socks proxy established on port 8123'
  end
end


function pxoff
  switch (uname)
    case Darwin
      networksetup -setsocksfirewallproxy "Wi-Fi" localhost 8123
      and networksetup -setsocksfirewallproxystate "Wi-Fi" off
      and networksetup -getsocksfirewallproxy "Wi-Fi"
      and echo '  💻   Socks proxy on port 8123 has been turned off'
  end
end


function myip
  printf '  🖥   Local IP: '
  ifconfig | grep -v '127.0.0.1' | awk '/inet / { print $2 }' | sed -e s/addr://
  curl -s ifconfig.co/json | python3 -m json.tool
end


function wifioff
  switch (uname)
    case Darwin
      networksetup -setairportpower en0 off
      and echo '  💻   The Wi-Fi is now off!'
      or echo '  ❗  There was an error'
  end
end


function wifion
  networksetup -setairportpower en0 on
  and echo '  💻   The Wi-Fi is now on!'
  or echo '  ❗  There was an error'
end


# Program aliases -------------------------------

alias vscconf=codeconf
alias confcode=codeconf
alias codeconf="pushd ~/Library/Application\ Support/Code/User"

alias saf=safari
function safari
  switch (uname)
    case Darwin
      open -a "Safari" https://$argv[1]
  end
end


alias chr=chrome
function chrome
  switch (uname)
    case Darwin
      open -a "Google Chrome" https://$argv[1]
  end
end


alias ff=firefox
function firefox
  switch (uname)
    case Darwin
      open -a "Mozilla Firefox" https://$argv[1]
    case Linux
      firefox $argv
  end
end

# Config ----------------------------------------

function setup_omf
  echo ' 🐠   Installing z and eclm theme... '
  omf install z
  omf install eclm
  omf theme eclm
end

# brew
set -g fish_user_paths "/usr/local/bin" $HOME"/.cargo/bin" $fish_user_paths
