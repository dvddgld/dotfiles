""" Show status of git repositories in a directory """

import os
import subprocess


def main():
    """ Main """
    directory = os.path.join(os.path.expanduser('~'), 'code')
    get_git_status(directory)


def get_git_status(root_dir):
    """ Run 'git status' on all repos found recursively in a folder """
    existing = set()
    for root, dirs, files in os.walk(root_dir, topdown=True):
        if any(root.startswith(r) for r in existing):
            continue
        if '.git' in dirs:
            repo_dir = root + os.sep
            existing.add(repo_dir)
            relative_dir = repo_dir.replace(root_dir, '')
            title = ' {} '.format(relative_dir)
            print(title)
            repo_status = subprocess.run(
                ['git', 'status', '--short', '--branch'], cwd=repo_dir)
            print('\n')


def colored(text):
    return '\x1b[6;30;42m' + text + '\x1b[0m'


if __name__ == '__main__':
    main()
