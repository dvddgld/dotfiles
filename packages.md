# shells
fish
zsh


# git
git
hub

# tools
mercurial
nano
vim
p7zip


# lang
python 
python3
ruby
golang


## Node 
n (use n to install node)
yarn


# DB
sqlite
mysql
mariadb
postgresql
mongo


# other
redis
elasticsearch


## GUI packages
iterm2
hyper

atom
visual-studio-code

firefox
google-chrome

mysql-workbench

virtualbox

libreoffice
paintbrush
