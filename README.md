
# dotfiles

```bash
# Caution: this will overwrite your configs

# To install, simply run
source https://raw.githubusercontent.com/dvddgld/dotfiles/master/setup.sh
```



### Browser Setup

##### General extensions
* uBlock origin        -   Help block malicious code

##### Dev extensions
* Pathogen             -   Outline every element with different colours
* React devtools       -   Debug React apps
* Vue devtools         -   Debug Vue apps
