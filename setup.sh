#!/usr/bin/env bash

function main() {
  echo
  echo ' 💻   Welcome to dvddcfg, the quickest route to pleasant programming'
  echo
  create_folders
  install_xcode_tools
  install_brew
  install_fish
  set_fish_as_login_sh
  install_omf
  install_dev_packages
  link_config_files
}

function create_folders() {
  mkdir -p ~/.config/dvddcfg/log/
  mkdir -p ~/.config/dvddcfg/scripts/
}

function install_xcode_tools() {
  printf ' 🛠️   Installing xcode cli tools... '
  xcode-select --install &>~/.config/dvddcfg/log/xcode.log
  echo ' ✔️ '
}

function install_brew() {
  printf ' 📦   Installing homebrew... '

  # If on macOS, and brew isn't installed, install brew
  if is_mac; then
    if ! type brew &>/dev/null; then
      /usr/bin/ruby -e "\$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" &>~/.config/dvddcfg/log/brew-install.log
    fi
  fi

  echo ' ✔️ '
}

function install_fish() {
  printf ' 🐠   Installing Friendly Interactive SHell... '

  # If on macOS, install fish 
  is_mac && brew install fish &>/dev/null

  echo ' ✔️ '
}

function set_fish_as_login_sh() {
  printf ' 🐠   Setting fish as your login shell... '
  if [[ ! $SHELL =~ .*fish ]]; then
    chsh -s `which fish`
  fi
  echo ' ✔️ '
}


function install_omf() {
  printf ' 🐠   Installing omf... '
  if ! type -t omf &>/dev/null; then
    (curl -sL https://get.oh-my.fish | fish &>/dev/null) &
  fi
  echo ' ✔️ '
}


function install_dev_packages() {
  printf ' 📦   Installing homebrew packages... '
  p_install_dev  &>~/.config/dvddcfg/log/brew-packages.log
  p_install_node &>~/.config/dvddcfg/log/n.log
  p_install_npm_modules &>~/.config/dvddcfg/log/yarn.log
  echo ' ✔️ '
}

p_install_dev() {
  if is_mac; then
    brew install z 
    brew install openssh mosh 
    brew install nano vim 
    brew install p7zip
    brew install git mercurial
    brew install gcc go
    brew install python python3
    brew install cocoapods
    brew install n yarn
    brew install sqlite postgresql

    brew cask install google-chrome
    brew cask install firefox
    brew cask install iterm2
    brew cask install powershell
    brew cask install visual-studio-code
  elif is_linux; then
    apt update
    apt install build-essential libssl-dev libffi-dev
    apt install git-core mercurial
    apt install golang-go
    apt install python3-dev python3-pip python-dev
  fi
}

function p_install_node() {
  if ! type -t n &>/dev/null; then
    n latest
    n lts
  fi
}

function p_install_npm_modules() {
  if ! type -t yarn &>/dev/null; then
    yarn global add eslint
    yarn global add create-react-app 
    yarn global add create-react-native-app
    yarn global add react-native-cli
  fi
}

function link_config_files() {
  printf ' 📄   Linking config files...'

  # Get repo
  # TODO

  # Link fish config
  if is_mac && [ ! -f ~/.config/fish/config.fish ] ; then
    ln -fs "$(pwd)/src/config.fish" $HOME"/.config/fish/config.fish"
    ln -fs "$(pwd)/src/scripts/git_status.py" $HOME"/.config/dvddcfg/scripts/git_status.py"
  fi

  # Copy git config
  if [ ! -f ~/.gitconfig ] ; then
    cp "$(pwd)/src/gitconfig" $HOME"/.gitconfig"

    # Set git name and email
    printf 'Git config: what is your name? '
    read gcname
    git config --global --replace-all user.name "$gcname"
    printf 'Git config: what is your email? '
    read gcemail
    git config --global --replace-all user.email $gcemail
  fi

  # Link vscode config
  if is_mac ; then
    printf ''
    # 'TODO'
  fi

  echo ' ✔️ '
}


function is_mac() {
  [ `uname -s` == Darwin ] && return 0 || return 1
}


function is_linux() {
  [ `uname -s` == Linux ] && return 0 || return 1
}


main